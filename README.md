Fork from https://github.com/dwm9100b/lswRichTextEditors.git by Dale Morrison

dwm9100b committed on May 29, 2014

How to integrate rich text editors into a LightSwitch HTML Client


# lswRichTextEditors

## Sample project showing how to integrate 3 Rich Text Editors into a LightSwitch HTML application.

The project works with the following:
* ckEditor
* TinyMCE
* Kendo UI

You can test drive the project at: http://lswRichTextEditors.samplebuild.com/